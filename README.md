# Git config

## git aliases

```sh
git config --global -l | grep alias.
```

```sh
alias.fpush=push --force-with-lease
alias.ffmerge=merge --ff-only
alias.switchc=switch -c
alias.oreset=!f() { read -p "Confirm reset to origin [y/n] : " prompt; [ "${prompt,,}" = y ] && git reset --hard origin/$(git branch --show-current) || echo "Git reset aborted."; }; f;
alias.foreset=!f() { git fetch && git oreset }; f;
alias.publish=!f() { read -p "Confirm publish to origin [y/n] : " prompt; [ "${prompt,,}" = y ] && git push --set-upstream origin $(git branch --show-current) || echo "Git publish aborted."; }; f;
```

```sh
git config --global alias.fpush 'push --force-with-lease'
git config --global alias.ffmerge 'merge --ff-only'
git config --global alias.switchc 'switch -c'
git config --global alias.oreset '!f() { read -p "Confirm reset to origin [y/n] : " prompt; [ "${prompt,,}" = y ] && git reset --hard origin/$(git branch --show-current) || echo "Git reset aborted."; }; f;'
git config --global alias.foreset "!f() { git fetch && git oreset }; f;"
git config --global alias.publish '!f() { read -p "Confirm publish to origin [y/n] : " prompt; [ "${prompt,,}" = y ] && git push --set-upstream origin $(git branch --show-current) || echo "Git publish aborted."; }; f;'
```